import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HistoryComponent} from './history/history.component';
import {BalanceComponent} from './balance/balance.component';
import {NetworkProblemComponent} from './network-problem/network-problem.component';
import {NetworkGuard} from './network.guard';
import {TransferComponent} from './transfer/transfer.component';
import {AccountComponent} from './account/account.component';

const appRoutes: Routes = [
  {
    path: '',
    canActivateChild: [NetworkGuard],
    children: [
      {path: '', redirectTo: '/account', pathMatch: 'full'},
      {path: 'account', component: AccountComponent},
      {path: 'history', component: HistoryComponent},
      {path: 'balance', component: BalanceComponent},
      {path: 'transfer', component: TransferComponent}
    ]
  },
  {path: '**', component: NetworkProblemComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
