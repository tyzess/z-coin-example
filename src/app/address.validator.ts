import {AbstractControl} from '@angular/forms';
import {Web3Service} from './web3.service';

export class AddressValidator {

  static validAddress(web3Service: Web3Service) {
    return (control: AbstractControl) => {
      const validAddress = web3Service.isValidAddress(control.value);
      return validAddress ? null : {invalidAddress: true};
    };
  }
}
