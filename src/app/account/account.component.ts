import {Component, OnInit} from '@angular/core';
import {ContractService} from '../contract.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  balance: number;

  constructor(private contractService: ContractService) {
  }

  ngOnInit() {
    this.contractService.balance().subscribe(balance => this.balance = balance);
  }
}
