import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'zcoin'
})
export class ZCoinPipe implements PipeTransform {
  private decimal = Math.pow(10, 18);

  transform(value: number, args?: any): string {
    return `${value / this.decimal} ZCOIN`;
  }
}
