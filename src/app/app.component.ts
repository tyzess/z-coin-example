import {Component} from '@angular/core';
import {Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loading: boolean;

  constructor(private router: Router) {
    this.router.events.subscribe(event => this.isLoading(event));
  }

  private isLoading(event: Event): void {
    switch (event.constructor) {
      case NavigationStart:
        this.loading = true;
        break;
      case NavigationEnd:
      case NavigationError:
      case NavigationCancel:
        this.loading = false;
    }
  }
}
