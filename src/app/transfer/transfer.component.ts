import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ContractService} from '../contract.service';
import {Web3Service} from '../web3.service';
import {AddressValidator} from '../address.validator';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit, OnDestroy {
  form: FormGroup;
  messages: string[];
  private subscription: Subscription;

  constructor(private fb: FormBuilder,
              private contractService: ContractService,
              private web3Service: Web3Service) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      address: this.fb.control('', AddressValidator.validAddress(this.web3Service)),
      amount: this.fb.control(0, Validators.min(1))
    });
  }

  onSubmit() {
    this.unsubscribeIfNecessary();
    this.messages = [];
    const address = this.form.controls.address.value;
    const amount = this.form.controls.amount.value;
    this.subscription = this.contractService.transfer(address, amount).subscribe(promiEvent => promiEvent
      .on('transactionHash', hash => this.messages.push(`Transaction with hash ${hash} was created`))
      .on('confirmation', number => this.messages.push(`Confirmation ${number} arrived`))
      .on('receipt', () => this.messages.push(`Receipt is finally here`)));
  }

  ngOnDestroy(): void {
    this.unsubscribeIfNecessary();
  }

  private unsubscribeIfNecessary() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
