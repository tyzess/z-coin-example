import {Injectable} from '@angular/core';
import {Contract} from 'web3-eth-contract';
import Web3 from 'web3';
import {from, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class Web3Service {
  private readonly web3: Web3;
  private readonly ethereum: any;

  constructor() {
    const nativeWindow: any = window;
    if (nativeWindow.ethereum) {
      this.ethereum = nativeWindow.ethereum;
      this.web3 = new Web3(nativeWindow.ethereum);
    }
  }

  hasAccessToMetamask(): Observable<boolean> {
    if (!this.ethereum) {
      return of(false);
    }
    return from(this.ethereum.enable()).pipe(
      map(() => true),
      catchError(() => of(false))
    );
  }

  loadContract(abi: any, address: string): Contract {
    return new this.web3.eth.Contract(abi, address);
  }

  isValidAddress(address: string): boolean {
    return this.web3.utils.isAddress(address);
  }

  getAccounts(): Observable<string[]> {
    return from(this.web3.eth.getAccounts());
  }

  getAccount(): Observable<string> {
    return this.getAccounts().pipe(map((accounts: any[]) => accounts[0]));
  }
}
