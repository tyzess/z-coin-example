import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ContractService} from '../contract.service';
import {Web3Service} from '../web3.service';
import {AddressValidator} from '../address.validator';
import {debounceTime, filter, map, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})
export class BalanceComponent implements OnInit {
  form: FormGroup;
  address: FormControl;

  balance: number;

  constructor(private fb: FormBuilder,
              private contractService: ContractService,
              private web3Service: Web3Service) {
  }

  ngOnInit(): void {
    this.address = this.fb.control('', AddressValidator.validAddress(this.web3Service));
    this.form = this.fb.group({address: this.address});

    this.form.controls.address.valueChanges.pipe(
      debounceTime(300),
      filter(() => this.form.valid),
      map(() => this.form.controls.address.value),
      switchMap(address => this.contractService.balanceOf(address))
    ).subscribe(balance => this.balance = balance);
  }
}
