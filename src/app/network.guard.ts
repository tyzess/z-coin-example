import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Web3Service} from './web3.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ContractService} from './contract.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkGuard implements CanActivate, CanActivateChild {

  constructor(private web3Service: Web3Service,
              private contractService: ContractService,
              private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.web3Service.hasAccessToMetamask().pipe(
      map(accessGranted => {
          if (accessGranted) {
            return true;
          }
          this.router.navigate(['/oups']);
        }
      )
    );
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }
}
