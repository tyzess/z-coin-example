import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Contract} from 'web3-eth-contract';
import {Web3Service} from './web3.service';
import {map, switchMap, tap} from 'rxjs/operators';
import {PromiEvent} from 'web3-core';

@Injectable({
  providedIn: 'root'
})
export class ContractService {
  private contract: Contract;
  private address = '0x97b863c5ebf708725a9bb5c88fbd08de935ef2b9';

  constructor(private http: HttpClient, private web3Service: Web3Service) {
  }

  init(): Promise<any> {
    return this.loadAbi().pipe(
      map(abi => this.web3Service.loadContract(abi, this.address)),
      tap(contract => this.contract = contract),
    ).toPromise();
  }

  loadAbi(): Observable<any> {
    return this.http.get('assets/abi.json');
  }

  balance(): Observable<number> {
    return this.web3Service.getAccount().pipe(switchMap(account => this.balanceOf(account)));
  }

  balanceOf(address: string): Observable<number> {
    return from(this.contract.methods.balanceOf(address).call());
  }

  transfer(address: string, amount: number): Observable<PromiEvent<any>> {
    return this.web3Service.getAccount().pipe(
      map(account => this.contract.methods.transfer(address, amount).send({from: account}))
    );
  }

  transfers(): any {
    return this.contract.events.Transfer({fromBlock: 0});
  }
}

export function initializeContract(contractService: ContractService) {
  return (): Promise<Contract> => contractService.init();
}
