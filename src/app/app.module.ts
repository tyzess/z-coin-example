import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ContractService, initializeContract} from './contract.service';
import {AppComponent} from './app.component';
import {HistoryComponent} from './history/history.component';
import {BalanceComponent} from './balance/balance.component';
import {NetworkProblemComponent} from './network-problem/network-problem.component';
import {TransferComponent} from './transfer/transfer.component';
import {AccountComponent} from './account/account.component';
import {ZCoinPipe} from './z-coin.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HistoryComponent,
    BalanceComponent,
    NetworkProblemComponent,
    TransferComponent,
    AccountComponent,
    ZCoinPipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [
    {provide: APP_INITIALIZER, useFactory: initializeContract, deps: [ContractService], multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
