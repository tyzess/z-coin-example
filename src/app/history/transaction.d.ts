export interface TransactionEvent {
  returnValues: {
    from: string;
    to: string;
    tokens: number
  };
  raw: {
    data: string,
    topics: string[]
  };
  event: string;
  signature: string;
  logIndex: number;
  transactionIndex: number;
  transactionHash: string;
  blockHash: string;
  blockNumber: number;
  address: string;
}
