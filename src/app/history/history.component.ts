import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ContractService} from '../contract.service';
import {TransactionEvent} from './transaction';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  transfers: TransactionEvent[] = [];

  constructor(private contractService: ContractService, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.contractService.transfers()
      .on('data', transfer => {
        this.transfers.push(transfer);
        this.changeDetectorRef.detectChanges();
      })
      .on('changed', console.error)
      .on('error', console.error);
  }
}
